function obtenerDatos(e) {
  const url = "http://localhost:4000/users";

  e.preventDefault();

  const form = document.getElementById("form");

  const name = form.name.value;
  const email = form.email.value;
  const username = form.username.value;
  const numberPhone = form.telefono.value;
  const pass = form.pass.value;

//   if (!name || !username || !numberPhone || !pass || !email) {
//     alert("Todos los campos son requeridos");
//     return false;
//   }

  const formData = {
    name: name,
    email: email,
    username: username,
    numberPhone: numberPhone,
    password: pass,
  };

  console.log("registrar usuario");

  fetch(url, {
    method: "POST",
    body: JSON.stringify(formData),
    headers: { "Content-Type": "application/json" },
  })
    .then((r) => r.json().then((data) => ({ status: r.status, body: data })))
    .then((info) => {
      console.log(info);
      if (info.status === 200 || info.status === 201) {
        console.log("status: "+info.status)
        alert("Se ha registrado exitosamente")
        const messagge = document.getElementById("messagge");
        messagge.innerHTML = "Se ha registrado exitosamente";
      }
      else{
        const messagge = document.getElementById("messagge");
        messagge.innerHTML = "Error:"+info.body.message;
      } 
    })
    .catch(error=>{
        const messagge = document.getElementById("messagge");
        messagge.innerHTML = "Error:";
        console.log(error)
        alert(error)
    })
}

